#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file_ops.h"
#include "io.h"
#include "uart.h"
#include "version.h"

static const uint32_t BOOTLOADER_MAGIC_CODE = 0x47573100;

typedef enum bootloader_commands {
  BOOTLOADER_CMD_GET_VERSION = 0,
  BOOTLOADER_CMD_LOAD_IMAGE,
  BOOTLOADER_CMD_BOOT_IMAGE,
} bootloader_commands_t;

typedef enum bootloader_command_status {
  BOOTLOADER_CMD_STATUS_OK = 0,
  BOOTLOADER_CMD_STATUS_PAYLOAD_CRC_FAIL,
  BOOTLOADER_CMD_STATUS_PAYLOAD_TOO_LARGE,
  BOOTLOADER_CMD_STATUS_UNRECOGNISED,
  BOOTLOADER_CMD_STATUS_IO_ERROR,
  BOOTLOADER_CMD_STATUS_MAGIC_NO_MATCH,
} bootloader_command_status_t;

typedef struct bootloader_response_header {
  uint32_t magic;
  uint32_t payload_length;
  uint32_t payload_crc;
  uint32_t id;
  uint32_t status;
} bootloader_response_header_t;

typedef struct bootloader_command_header {
  uint32_t magic;
  uint32_t payload_length;
  uint32_t payload_crc;
  uint32_t id;
} bootloader_command_header_t;

static const char* bootloader_command_status_strings[] = {
  "ok",
  "payload crc failed",
  "payload too large",
  "unrecognised command",
  "io error",
  "magic code does not match"
};

#define IO_CHUNK_SIZE_BYTES 200

static int handle;

static void print_help(void)
{
  fprintf(stdout, "gw1_bl_tool v%d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
  fprintf(stdout, "Usage: gw1_bl_tool /path/to/binary.bin [/dev/ttyUSBx] [baud rate]\n");
}

static bool io_write(const uint8_t* buff, size_t buff_length)
{
  int ret = uart_write(handle, buff, buff_length);
  if (ret == 0) {
    return true;
  }

  fprintf(stderr, "IO write failed %i\n", ret);

  return false;
}

static bool io_read(uint8_t* buff, size_t buff_length)
{
  int ret = uart_read(handle, buff, buff_length);
  if (ret == (int)buff_length) {
    return true;
  } else if (ret == 0) {
    fprintf(stderr, "IO timeout\n");
  } else {
    fprintf(stderr, "IO read failed %i\n", ret);
  }

  return false;
}

static bool send_bootloader_command(io_t* io, bootloader_commands_t command, uint32_t crc, uint32_t length)
{
  bootloader_command_header_t header = {
    .magic = BOOTLOADER_MAGIC_CODE,
    .id = command,
    .payload_crc = crc,
    .payload_length = length
  };

  return io->io_write((uint8_t*)&header, sizeof(header));
}

static bool get_bootloader_response(io_t* io)
{
  bootloader_response_header_t resp;
  memset(&resp, 0, sizeof(bootloader_response_header_t));
  bool io_state = io->io_read((uint8_t*)&resp, sizeof(resp));

  if (io_state) {
    if ((resp.status == BOOTLOADER_CMD_STATUS_OK) && (resp.magic == BOOTLOADER_MAGIC_CODE)) {
      if (resp.payload_length > 0) {
        uint8_t* buff = malloc(resp.payload_length);
        io->io_read(buff, resp.payload_length);
        fprintf(stdout, "%s\n", (char*)buff);
        free(buff);
      }
      return true;
    } else {
      fprintf(stderr, "Incorrect response received, status: %s\n", bootloader_command_status_strings[resp.status]);
    }
  }

  return false;
}

static bool boot_bootloader_image(io_t* io)
{
  send_bootloader_command(io, BOOTLOADER_CMD_BOOT_IMAGE, 0, 0);

  bool bootloader_response = get_bootloader_response(io);
  fprintf(stdout, "Booting image %s\n", (bootloader_response ? "OK" : "Failed"));

  return bootloader_response;
}

static bool load_bootloader_image(io_t* io, const char* binary_file)
{
  assert(binary_file);

  void* file = file_open(binary_file);
  if (file) {

    uint32_t crc = file_crc32(file);
    uint32_t length = file_size(file);

    // Generate header
    send_bootloader_command(io, BOOTLOADER_CMD_LOAD_IMAGE, crc, length);

    // Send payload
    uint8_t tmp_buff[IO_CHUNK_SIZE_BYTES];

    while (length != 0) {
      size_t actual_read = file_read(file, tmp_buff, sizeof(tmp_buff));
      io->io_write(tmp_buff, actual_read);
      length -= actual_read;
    }

    file_close(file);

    // Get response
    bool bootloader_response = get_bootloader_response(io);
    fprintf(stdout, "Loading image %s\n", (bootloader_response ? "OK" : "Failed"));

    return bootloader_response;

  } else {
    fprintf(stderr, "%s doesn't exist\n", binary_file);
  }
  return false;
}

static bool request_bootloader_version(io_t* io)
{
  send_bootloader_command(io, BOOTLOADER_CMD_GET_VERSION, 0, 0);

  fprintf(stdout, "GW1 Bootloader version: ");
  bool bootloader_response = get_bootloader_response(io);

  if (!bootloader_response) {
    fprintf(stdout, "Retrieving device bootloader version failed\n");
  }

  return bootloader_response;
}

int main(int argc, char const* argv[])
{
  if (argc != 4) {
    print_help();
    return 1;
  }

  const char* device = "/dev/ttyUSB";
  char device_str[20];
  memset(device_str, 0, sizeof(device_str));

  const char* usb_tty_index = argv[2];
  strncat(device_str, device, 12);
  strncat(device_str, usb_tty_index, 3);

  size_t baud_rate = atol(argv[3]);

  handle = uart_open(device_str, baud_rate);
  if (handle < 0) {
    return 1;
  }

  const char* binary_file = argv[1];

  io_t io = {
    .io_read = io_read,
    .io_write = io_write,
  };

  if (request_bootloader_version(&io)) {
    if (load_bootloader_image(&io, binary_file)) {
      boot_bootloader_image(&io);
    }
  }

  return 0;
}
