#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "crc32.h"

#include "file_ops.h"

void* file_open(const char* filename)
{
  assert(filename);
  return (void*)fopen(filename, "r");
}

void file_close(void* fp)
{
  assert(fp);
  fclose(fp);
}

void file_reset(void* fp)
{
  assert(fp);
  fseek(fp, 0, SEEK_SET);
}

uint32_t file_crc32(void* fp)
{
  size_t size = file_size(fp);
  uint8_t* fp_contents = malloc(size);

  file_read(fp, fp_contents, size);
  uint32_t crc = xcrc32(fp_contents, size, 0);
  free(fp_contents);
  file_reset(fp);

  return crc;
}

size_t file_size(void* fp)
{
  assert(fp);

  fseek(fp, 0, SEEK_END);
  size_t size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  return size;
}

size_t file_read(void* fp, uint8_t* payload, size_t payload_length)
{
  assert(fp);
  assert(payload);

  return fread(payload, sizeof(*payload), payload_length, fp);
}
