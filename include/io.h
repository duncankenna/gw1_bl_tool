#ifndef IO_H
#define IO_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct io {
  bool (*io_write)(const uint8_t* buff, size_t buff_length);
  bool (*io_read)(uint8_t* buff, size_t buff_length);
} io_t;

#endif /* IO_H */
