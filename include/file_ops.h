#ifndef FILE_OPS_H
#define FILE_OPS_H

#include <stddef.h>
#include <stdint.h>

void* file_open(const char* filename);
void file_close(void* fp);
void file_reset(void* fp);
uint32_t file_crc32(void* fp);
size_t file_size(void* fp);
size_t file_read(void* fp, uint8_t* payload, size_t payload_length);

#endif /* FILE_OPS_H */
