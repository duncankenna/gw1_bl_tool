#ifndef UART_H
#define UART_H

#include <stddef.h>
#include <stdint.h>

int uart_open(const char* device, uint32_t baud_rate);
int uart_close(int fd);
int uart_write(int fd, const uint8_t* buffer, size_t size);
int uart_read(int fd, uint8_t* buffer, size_t size);

#endif /* UART_H */
